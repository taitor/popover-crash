//
//  ContentView.swift
//  PopoverCrash
//
//  Created by Taito Ri on 2020/06/28.
//  Copyright © 2020 Taito Ri. All rights reserved.
//

import SwiftUI

enum Item: Identifiable, CaseIterable {
  case item1

  var id: Self { self }
  var name: String {
    switch self {
    case .item1:
      return "item1"
    }
  }
}

struct ContentView: View {
  @State private var popoverItem: Item?

  var body: some View {
    Button("Push me") {
      self.popoverItem = .item1
    }
    .popover(item: $popoverItem) {
      Text($0.name)
    }
  }
}
